FROM node:lts-alpine
WORKDIR /usr/src/app
COPY . .
ENV NODE_OPTIONS=--openssl-legacy-provider
RUN apk add --update --no-cache python3 make g++ gcc
RUN npm install
RUN npm run build
